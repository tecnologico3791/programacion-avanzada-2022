#!/usr/bin/env python3
#from persona import Persona

class Estudiante():

    def __init__(self):
        self._nombre = "ESTUDIANTE"

    @property
    def nombre(self):
        return self._nombre

    @nombre.setter
    def nombre(self, n):
        if isinstance(n, str):
            self._nombre = n
        else:
            return False
