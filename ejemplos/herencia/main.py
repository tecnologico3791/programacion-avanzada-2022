from estudiante import Estudiante
from ayudante import Ayudante
from persona import Persona
pepito = Estudiante()
pepito.nombre = "Pepito"
print(pepito.nombre)
ayudante = Ayudante()
# ayudante.nombre = "Diego"
print(ayudante.nombre)
print(type(ayudante))
if isinstance(ayudante, Persona):
    print("Si")
else:
    print("No")
ayudante.saluda()
