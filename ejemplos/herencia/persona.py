#!/usr/bin/env python3


class Persona():

    def __init__(self):
        self._nombre = "PERSONA"

    @property
    def nombre(self):
        return self._nombre

    @nombre.setter
    def nombre(self, n):
        if isinstance(n, str):
            self._nombre = n
        else:
            return False

    def saluda(self):
        print("Hola")
