#!/usr/bin/env python3

class Planeta:
    def __init__(self, nombre=None, masa=None, densidad=None,
                 diametro=None, distancia_sol=None, id_planeta=0):
        self._nombre_planeta = nombre
        self._masa = masa
        self._densidad = densidad
        self._diametro = diametro
        self._distancia_sol = distancia_sol
        self._id_planeta = id_planeta

    @property
    def nombre_planeta(self):
        return self._nombre_planeta

    @nombre_planeta.setter
    def nombre_planeta(self, nombre):
        if isinstance(nombre, str):
            self._nombre_planeta = nombre
        else:
            raise TypeError("Solo se permiten textos")


    @property
    def masa(self):
        return self._masa

    @masa.setter
    def masa(self, numero):
        if isinstance(numero, int):
            self._masa = numero
        else:
            raise TypeError("Solo se permiten números")


    def muestra_caracteristicas(self):
        print(f"{self.nombre_planeta}, tiene una masa de {self.masa}")
        print(f"la densidad es: {self.densidad}, el diametro: {self.diametro}")
        print(f"tiene un distancia de {self.distancia_sol} del sol")


if __name__ == "__main__":
    tierra = Planeta(nombre="Tierra",
                     masa=123456,
                     densidad=12345,
                     diametro=12345,
                     distancia_sol=12345,
                     id_planeta=12345)

    tierra.nombre_planeta = "Tierra linda"
    tierra.masa = 100
    print(tierra.nombre_planeta)
