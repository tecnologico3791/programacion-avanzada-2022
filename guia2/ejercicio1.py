#!/usr/bin/env python3
""" Crear una clase para un “Perro” necesita ser sacado a pasear. Este
perro solo saldrá si tomó un agua hace más de 4 horas. En un archivo
perro.py y como parte de la clase ya mencionada se tienen tres
métodos: set tomar agua(int), que permite especificar el tiempo
(horas) con un número entero; get hora toma agua(), que devuelve la
hora de en que bebió agua el perro; y caminar(), que si es verdadero
(True) indicará si el perro necesita salir a pasear, caso contrario
será falso (False).  Condición, el perro no puede tomar agua si está
paseando.  Crear un menú de interacción para el Perro y el tiempo
transcurrido para que sea sacado a pasear.
"""

import random

class Perro:
    def __init__(self):
        self.tomo_agua = None

    def get_hora_tomo_agua(self):
        return self.tomo_agua

    def set_tomar_agua(self, hora):
        self.tomo_agua = hora

    def caminar(self):
        if self.tomo_agua > 4:
            return True
        return False


if __name__ == "__main__":
    tobi = Perro()
    tobi.set_tomar_agua(random.randint(0, 10))
    for i in range(10):
        if not tobi.caminar():
            toma_agua = random.randint(0,4)
            tobi.set_tomar_agua(tobi.get_hora_tomo_agua() + toma_agua)
            print("Tobi está tomando agua")
        else:
            tobi.set_tomar_agua(tobi.get_hora_tomo_agua() - 1)
            print("Tobi está paseando")
