#!/usr/bin/env python3
""" Crear una clase para identificar vacunas. Se deberá crear un
constructor que deberá tener como argumentos: el nombre de la vacuna
y el laboratorio que la elaboró, ambos de tipo strings. Se deberá
crear un método en el que se agregarán los efectos secundarios de cada
vacuna (set agrega efecto secundario()) y otro para mostrar los
efectos secundarios de la vacuna. Los efectos secundarios deben ser
almacenados en una lista ([]) y el retorno de los valores deben ser
mostrados linea a linea. Cree a lo menos 3 objetos de tipo Vacuna y
para cada una efectos secundarios."""

import random


lista_efectos = ["Dolor en la zona de vacunación", "Fiebre",
                 "Cefalea", "Mareos", "Mialgia (dolores musculares)",
                 "Artralgia (dolor en las articulaciones)"
                 "Cansancio", "Escalofríos", "Diarrea"]

class Vacuna:
    def __init__(self, nombre, laboratorio):
        self.nombre= nombre
        self.laboratorio = laboratorio
        self.efecto_secundario = []


    def agrega_efecto_secundario(self, efecto):
        self.efecto_secundario.append(efecto)

    def muestra_efecto_secundario(self):
        for efecto in self.efecto_secundario:
            print(f"{efecto}")


def efecto_secundarios(vacuna):
        # agrega tres efectos secundarios al azar
    numeros = [random.randint(0, len(lista_efectos) - 1) for x in range(3)]
    for item in numeros:
        vacuna.agrega_efecto_secundario(lista_efectos[item])


if __name__ == "__main__":
    vacunax = Vacuna("Vacuna X", "Laboratorio X")
    vacunay = Vacuna("Vacuna Y", "Laboratorio Y")
    vacunaz = Vacuna("Vacuna Z", "Laboratorio Z")
    efecto_secundarios(vacunax)
    efecto_secundarios(vacunay)
    efecto_secundarios(vacunaz)

    print(vacunax.nombre)
    vacunax.muestra_efecto_secundario()
    print(vacunay.nombre)
    vacunay.muestra_efecto_secundario()
    print(vacunaz.nombre)
    vacunaz.muestra_efecto_secundario()
