#!/usr/bin/env python3

class Planeta:
    def __init__(self, nombre=None, masa=None, densidad=None,
                 diametro=None, distancia_sol=None, id_planeta=0):
        self.nombre_planeta = nombre
        self.masa = masa
        self.densidad = densidad
        self.diametro = diametro
        self.distancia_sol = distancia_sol
        self.id_planeta = id_planeta

    def muestra_nombre(self):
        return self.nombre_planeta

    def muestra_caracteristicas(self):
        print(f"{self.nombre_planeta}, tiene una masa de {self.masa}")
        print(f"la densidad es: {self.densidad}, el diametro: {self.diametro}")
        print(f"tiene un distancia de {self.distancia_sol} del sol")


tierra = Planeta(nombre="Tierra",
                 masa=123456,
                 densidad=12345,
                 diametro=12345,
                 distancia_sol=12345,
                 id_planeta=12345)
print(tierra.muestra_nombre())
tierra.muestra_caracteristicas()
